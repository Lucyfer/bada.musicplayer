#include "PlayerExample.h"

using namespace Osp::Base;
using namespace Osp::Media;
using namespace Osp::Graphics;
using namespace Osp::Ui::Controls;

Player* pPlayer = null;
PlayerExampleListener* pListener = null;
BufferInfo bufferInfoforVideo;

PlayerExampleListener::PlayerExampleListener(void) {}

void PlayerExampleListener::OnPlayerOpened(result r) {
   pPlayer->Play();
}

void PlayerExampleListener::OnPlayerEndOfClip(void) {
   pPlayer->Close();
}

void PlayerExampleListener::OnPlayerBuffering(int percent) {}

void PlayerExampleListener::OnPlayerErrorOccurred(PlayerErrorReason r) {}

void PlayerExampleListener::OnPlayerInterrupted(void) {}

void PlayerExampleListener::OnPlayerReleased(void) {}

PlayerExample::PlayerExample() {}

PlayerExample::~PlayerExample() {}

result PlayerExample::Play(String file) {
	result r = E_SUCCESS;
	pListener = new PlayerExampleListener;
	pPlayer = new Player();
	r = pPlayer->Construct(*pListener, 0);
	if (IsFailed(r))
		goto CATCH;
	r = pPlayer->OpenFile(file, true);
	if (IsFailed(r))
		goto CATCH;
CATCH:
	return r;
}

result PlayerExample::Pause() {
	return pPlayer->Pause();
}

result PlayerExample::Resume() {
	return pPlayer->Play();
}

result PlayerExample::Stop() {
	pPlayer->Close();
	return pPlayer->Stop();
}



