#include "Form1.h"

using namespace Osp::Base;
using namespace Osp::Base::Collection;
using namespace Osp::Ui;
using namespace Osp::Ui::Controls;
using namespace Osp::Media;
using namespace Osp::Graphics;
using namespace Osp::App;
using namespace Osp::Io;

Form1::Form1(void) {}

Form1::~Form1(void) {}

bool Form1::Initialize() {
	Construct(L"IDF_FORM1");
	return true;
}

result Form1::OnInitializing(void) {
	result r = E_SUCCESS;
	pButtonPlay = static_cast<Button *>(GetControl(L"IDC_BUTTON_PLAY"));
	pButtonPrev = static_cast<Button *>(GetControl(L"IDC_BUTTON_PREV"));
	pButtonNext = static_cast<Button *>(GetControl(L"IDC_BUTTON_NEXT"));
	pListPlaylist = static_cast<List *>(GetControl(L"IDC_PLAYLIST"));
	playlist = new ArrayListT<String>();
	playlist->Construct();
	currentIndex = 0;
	isPlaying = false;
	bm32 = Application::GetInstance()->GetAppResource()->GetBitmapN(L"icon32.png");
	playEx = new PlayerExample();

	if (pButtonPlay != null) {
		pButtonPlay->SetActionId(ID_BUTTON_PLAY);
		pButtonPlay->AddActionEventListener(*this);
	}

	if (pButtonPrev != null) {
		pButtonPrev->SetActionId(ID_BUTTON_PREV);
		pButtonPrev->AddActionEventListener(*this);
	}

	if (pButtonNext != null) {
		pButtonNext->SetActionId(ID_BUTTON_NEXT);
		pButtonNext->AddActionEventListener(*this);
	}

	if (pListPlaylist != null) {
		Directory d;
		String base(L"/Res/Share/");
		d.Construct(base);
		DirEnumerator* de = d.ReadN();
		if (de) {
			de->MoveNext();
			while (de->MoveNext() == E_SUCCESS) {
				DirEntry entry = de->GetCurrentDirEntry();
				if (entry.IsDirectory())
					continue;
				String p(base);
				p += entry.GetName();
				playlist->Add(p);
				pListPlaylist->AddItem(&String(entry.GetName()), &String(L""), bm32, bm32, 110);
			}
		}
		else AppLog("Error on reading folder");
	}
	return r;
}

result Form1::OnTerminating(void) {
	result r = E_SUCCESS;
	return r;
}

void Form1::PlayerChangeState() {
	String s;
	if (isPlaying) {
		playEx->Stop();
		isPlaying = false;
	}
	else if (playlist->GetAt(currentIndex, s) == E_SUCCESS) {
		isPlaying = true;
		playEx->Stop();
		playEx->Play(s);
	}
	else AppLog("Error invalid index");
}

void Form1::PlayerPlay() {
	String s;
	if (playlist->GetAt(currentIndex, s) == E_SUCCESS) {
		if (isPlaying)
			playEx->Stop();
		isPlaying = true;
		playEx->Play(s);
	}
	else AppLog("Error invalid index");
}

void Form1::OnActionPerformed(const Osp::Ui::Control& source, int actionId) {
	switch(actionId) {
		case ID_BUTTON_PLAY: {
			String s;
			if (isPlaying) {
				playEx->Stop();
				isPlaying = false;
			}
			else if (playlist->GetAt(currentIndex, s) == E_SUCCESS) {
				isPlaying = true;
				playEx->Play(s);
			}
			else AppLog("Error invalid index");
			break;
		}
		case ID_BUTTON_NEXT:
			if (currentIndex < playlist->GetCount() - 1)
				currentIndex++;
			PlayerPlay();
			break;
		case ID_BUTTON_PREV:
			if (currentIndex > 0)
				currentIndex--;
			PlayerPlay();
			break;
		default:
			break;
	}
}

