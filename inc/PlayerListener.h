#pragma once

#include <FBase.h>
#include <FMedia.h>

using namespace Osp::Media;

class PlayerListener : public Osp::Media::IPlayerEventListener
{
private:
	Player* _player;
public:
	PlayerListener();
	PlayerListener(Player* p) {
		_player = p;
	}
	virtual ~PlayerListener();

public:
	void OnPlayerOpened(result r) {};
	void OnPlayerEndOfClip(void) { _player->Stop(); };
	void OnPlayerBuffering(int percent) {};
	void OnPlayerErrorOccurred(Osp::Media::PlayerErrorReason r) {};
	void OnPlayerInterrupted(void) {};
	void OnPlayerReleased(void) {};
};

