#ifndef _FORM1_H_
#define _FORM1_H_

#include <FBase.h>
#include <FMedia.h>
#include <FUi.h>
#include <FGraphics.h>
#include <FIo.h>
#include <FApp.h>
#include "PlayerListener.h"
#include "PlayerExample.h"

class Form1 :
	public Osp::Ui::Controls::Form,
	public Osp::Ui::IActionEventListener
{

public:
	Form1(void);
	virtual ~Form1(void);
	bool Initialize(void);

protected:
	static const int ID_BUTTON_PLAY = 101;
	static const int ID_BUTTON_PREV = 102;
	static const int ID_BUTTON_NEXT = 103;
	static const int ID_PLAYLIST = 104;

	Osp::Ui::Controls::Button *pButtonPlay;
	Osp::Ui::Controls::Button *pButtonNext;
	Osp::Ui::Controls::Button *pButtonPrev;
	Osp::Ui::Controls::List *pListPlaylist;

	Osp::Graphics::Bitmap* bm32;
	Osp::Media::Player *player;
	PlayerListener *playerListener;
	Osp::Base::Collection::ArrayListT<Osp::Base::String> *playlist;
	int currentIndex;
	bool isPlaying;

	PlayerExample *playEx;

public:
	virtual result OnInitializing(void);
	virtual result OnTerminating(void);
	virtual void OnActionPerformed(const Osp::Ui::Control& source, int actionId);
	void PlayerChangeState(void);
	void PlayerPlay(void);
};

#endif	//_FORM1_H_
