#ifndef _PLAYER_EXAMPLE_H_
#define _PLAYER_EXAMPLE_H_

#include <FBase.h>
#include <FMedia.h>
#include <FGraphics.h>
#include <FUi.h>

class PlayerExample {
public:
	PlayerExample(void);
	virtual ~PlayerExample(void);
	result Play(Osp::Base::String);
	result Pause(void);
	result Resume(void);
	result Stop(void);	
};

class PlayerExampleListener
    : public Osp::Media::IPlayerEventListener
{
   public:
	PlayerExampleListener(void);
	void OnPlayerOpened(result r);
	void OnPlayerEndOfClip(void);
	void OnPlayerBuffering(int percent);
	void OnPlayerErrorOccurred(Osp::Media::PlayerErrorReason r);
	void OnPlayerInterrupted(void);
	void OnPlayerReleased(void);
};

#endif
